import socket
from _thread import *

list_of_clients = []
list_of_scores = []


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 2211))
s.listen()

pytania = ["Czy Epstein popelnil samobojstwo?\n",
           "Czy ladowanie na Ksiezycu bylo sfalszowane?\n",
           "Czy COVID istnieje?\n",
           "Czy szczepionki powodują autyzm?\n",
           "Czy Dalai Lama byl sponsorowany przez USA?\n",
           "Czy USA zatruwalo alkohol w czasie prohibicji?\n",
           "Czy 5G powoduje raka?\n",
           "Czy rzad zatruwa powietrze (chemtrails)\n",
           "Czy Ziemia jest plaska?\n",
           "Czy gry komputerowe powoduja ludobojstwa?\n", ]

odpowiedzi = ["NIE", "NIE", "TAK", "NIE", "TAK", "TAK", "NIE", "NIE", "NIE", "NIE"]


def thread(conn, addr):
    conn.send(("Jestes graczem numer " + str(list_of_clients.index(conn) + 1) + '\n').encode('utf-8'))
    if list_of_clients[0] == conn:
        while True:
            conn.send('Wpisz START aby rozpoczac'.encode('utf-8'))
            message = conn.recv(2048)
            if message == b'START':
                wiadomosc("Na pytanie odpowiadaj TAK lub NIE (wielkosc liter ma znaczenie)\n".encode("utf-8"))
                quiz()
                break
    kara = 1
    odpowiedz = ""
    while True:
        message = conn.recv(2048)
        if (message.decode('utf-8') != "TAK") and (message.decode('utf-8') != "NIE"):
            conn.send("ERROR".encode('utf-8'))
        elif len(pytania) == 10:
            conn.send("Oszustwo\n".encode('utf-8'))
            licznik(conn, 0)
        else:
            stara_odpowiedz = odpowiedz
            odpowiedz = odpowiedzi[0]
            if (stara_odpowiedz == odpowiedz) and (kara == 3):
                conn.send("Pauza\n".encode('utf-8'))
            elif message.decode('utf-8') == odpowiedz:
                kara = 1
                licznik(conn, 1)
                wiadomosc(
                    ("Poprawnej odpowiedzi udzielil gracz numer " + str(list_of_clients.index(conn) + 1) + '\n').encode(
                        'utf-8'))
                quiz()
                odpowiedzi.pop(0)
            else:
                kara = kara + 1
                licznik(conn, 0)
                conn.send('Niepoprawna odpowiedz'.encode('utf-8'))


def licznik(conn, i):
    pom = list_of_clients.index(conn)
    pom2 = list_of_scores[pom]
    if i == 1:
        list_of_scores[pom] = pom2 + 1
        conn.send(("Twoj wynik to " + str(list_of_scores[pom]) + " punkty \n").encode("utf-8"))
    elif i == 0:
        list_of_scores[pom] = pom2 - 2
        conn.send(("Twoj wynik to " + str(list_of_scores[pom]) + " punkty \n").encode('utf-8'))


def wygrany():
    wygrany_wynik = max(list_of_scores)
    wygrany_index = list_of_scores.index(wygrany_wynik)
    wiadomosc(("Quiz wygral gracz numer " + str(wygrany_index + 1) + " z wynikiem " + str(
        wygrany_wynik) + " punktow \n").encode('utf-8'))


def quiz():
    if not pytania:
        wygrany()
        wiadomosc("Quiz zostal zakonczony".encode('utf-8'))
    else:
        pytanie = pytania.pop(0)
        for connection in list_of_clients:
            connection.send(pytanie.encode('utf-8'))


def wiadomosc(message):
    for clients in list_of_clients:
        clients.send(message)


while True:

    if len(list_of_clients) < 4:
        conn, addr = s.accept()
        list_of_clients.append(conn)
        list_of_scores.append(0)
        print(addr[0] + " connected")
        start_new_thread(thread, (conn, addr))
