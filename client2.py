#!/usr/bin/env python3
import select
import socket
import sys


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


s.connect(('127.0.0.1', 2211))


while True:

    sockets_list = [sys.stdin, s]

    read_sockets, write_socket, error_socket = select.select(sockets_list, [], [])

    for socks in read_sockets:
        if socks == s:
            message = socks.recv(2048).decode('utf-8')
            print(message)
            if message == 'Wpisz START aby rozpoczac':
                message = sys.stdin.readline()
                s.sendall(message.rstrip().encode('utf-8'))
                sys.stdout.flush()
        else:
            message = sys.stdin.readline()
            s.sendall(message.rstrip().encode('utf-8'))
            sys.stdout.flush()